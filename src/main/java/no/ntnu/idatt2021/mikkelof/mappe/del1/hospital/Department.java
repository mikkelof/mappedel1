package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital;

import no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Class for the Department type.
 * @author Mikkel Ofrim
 * @version 1.0.0
 */

public class Department {
    private String departmentName;
    private ArrayList<Employee> employees = new ArrayList<Employee>();
    private ArrayList<Patient> patients = new ArrayList<Patient>();

    public Department(String departmentName) {
        this.departmentName = departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return  departmentName;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    /**
     * A method for adding employees to a department. If the employee is not already registered it will
     * be added to the employees ArrayList.
     * @param employee is the employee you want to add to the department
     */

    public void addEmployee(Employee employee) {
        for (Employee e : employees) {
            if(e.getSocialSecurityNumber().equals(employee.getSocialSecurityNumber())) {
                System.out.println("The employee is already registered");
                return;
            }
        }
        employees.add(employee);
        System.out.println("The employee was successfully registered");
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * A method for adding patients to a department. If the patient is not already registered it will
     * be added to the patients ArrayList.
     * @param patient
     */

    public void addPatient(Patient patient) {
        for (Patient p : patients) {
            if(p.getSocialSecurityNumber().equals(patient.getSocialSecurityNumber())) {
                System.out.println("The patient is already registered");
                return;
            }
        }
        patients.add(patient);
        System.out.println("The patient was successfully registered");
    }

    /**
     * Method that removes either a patient or an employee from a department. It will first check if the person
     * is an employee or a patient. After that it will remove the object from the correct ArrayList. If the person
     * is not registered in the correct ArrayList it will throw a RemoveException.
     * @param person
     * @throws RemoveException
     */

    public void remove (Person person) throws RemoveException {
        if (person instanceof Employee) {
            if ((employees.contains(person))) {
                employees.remove(person);
            } else {
                throw new RemoveException();
            }
        } else if (person instanceof Patient) {
            if ((patients.contains(person))) {
                patients.remove(person);
            } else {
                throw new RemoveException();
            }
        } else {
            throw new RemoveException();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return departmentName.equals(that.departmentName) && employees.equals(that.employees) && patients.equals(that.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }
}
