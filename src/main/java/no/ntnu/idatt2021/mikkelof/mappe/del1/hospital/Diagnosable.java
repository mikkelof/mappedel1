package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital;

/**
 * Interface for the Diagnosable
 * @author Mikkel Ofrim
 * @version 1.0.0
 */


public interface Diagnosable {
    void setDiagnosis(String diagnosis);
}
