package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital;

/**
 * Class for the Employee type.
 * @author Mikkel Ofrim
 * @version 1.0.0
 */


public class Employee extends Person {

    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString(){
        return "Employee name: " + getFullName() + "\nSocial security number: " + getSocialSecurityNumber();
    }
}
