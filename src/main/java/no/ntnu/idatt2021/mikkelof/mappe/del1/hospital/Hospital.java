package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital;

import java.util.ArrayList;

/**
 * Class for the Hospital type.
 * @author Mikkel Ofrim
 * @version 1.0.0
 */


public class Hospital {
    private final String hospitalName;

    private ArrayList<Department> departments = new ArrayList<Department>();

    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }

    public void addDepartment(Department department) {
        departments.add(department);
    }

    public String toString() {
        return "no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.Hospital name: " + hospitalName + "\nDepartments: " + departments;
    }
}
