package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital;

import no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.exception.RemoveException;

/**
 * Main class with the hospital client.
 * @author Mikkel Ofrim
 * @version 1.0.0
 */

public class HospitalClient {
    public static void main(String[] args) {
        Hospital stOlav = new Hospital("St. Olavs Hospital");
        HospitalTestData.fillRegisterWithTestData(stOlav);

        Department department = stOlav.getDepartments().get(0);
        Employee employee = department.getEmployees().get(0);
        Patient patient = new Patient("hduhd", "huuid", "7878474");

        //Tries removing a employee that is in the register - should work

        try {
            department.remove(employee);
            System.out.println("Employee has been removed");
        }
        catch (RemoveException e) {
            System.out.println("Employee could not be removed. Error: " + e.getMessage());
        }

        //Tries removing a patient that is not in the register - should not work and throw RemoveException

        try {
            department.remove(patient);
        }
        catch (RemoveException e) {
            System.out.println("Patient could not be removed. Error: " + e.getMessage());
        }
    }
}
