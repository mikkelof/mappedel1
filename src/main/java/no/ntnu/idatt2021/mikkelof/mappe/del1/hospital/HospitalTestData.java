package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital;

import no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.healthpersonal.Nurse;
import no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.healthpersonal.doctor.GeneralPractitioner;
import no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.healthpersonal.doctor.Surgeon;

/**
 * Class created just to add test data
 */

public final class HospitalTestData {
    private HospitalTestData() {
        // not called
    }
    /**
     * @param hospital The hospital you want to add the test data to
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.getEmployees().add(new Employee("Odd Even", "Primtallet", "12345"));
        emergency.getEmployees().add(new Employee("Huppasahn", "DelFinito", "23456"));
        emergency.getEmployees().add(new Employee("Rigmor", "Mortis", "34567"));
        emergency.getEmployees().add(new GeneralPractitioner("Inco", "Gnito", "45678"));
        emergency.getEmployees().add(new Surgeon("Inco", "Gnito", "56789"));
        emergency.getEmployees().add(new Nurse("Nina", "Teknologi", "67890"));
        emergency.getEmployees().add(new Nurse("Ove", "Ralt", "78901"));
        emergency.getPatients().add(new Patient("Inga", "Lykke", "89012"));
        emergency.getPatients().add(new Patient("Ulrik", "Smål", "90123"));
        hospital.getDepartments().add(emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployees().add(new Employee("Salti", "Kaffen", "09876"));
        childrenPolyclinic.getEmployees().add(new Employee("Nidel V.", "Elvefølger", "98765"));
        childrenPolyclinic.getEmployees().add(new Employee("Anton", "Nym", "87654"));
        childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene", "Sis", "76543"));
        childrenPolyclinic.getEmployees().add(new Surgeon("Nanna", "Na", "65432"));
        childrenPolyclinic.getEmployees().add(new Nurse("Nora", "Toriet", "54321"));
        childrenPolyclinic.getPatients().add(new Patient("Hans", "Omvar", "43210"));
        childrenPolyclinic.getPatients().add(new Patient("Laila", "La", "32109"));
        childrenPolyclinic.getPatients().add(new Patient("Jøran", "Drebli", "21098"));
        hospital.getDepartments().add(childrenPolyclinic);
    }
}