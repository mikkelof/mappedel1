package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital;

/**
 * Class for the Patient type.
 * @author Mikkel Ofrim
 * @version 1.0.0
 */


public class Patient extends Person implements Diagnosable {
    String diagnosis = "";

    protected String getDiagnosis() {
        return diagnosis;
    }

    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString(){
        return "Patient name: " + getFullName() + "\nSocial security number: " + getSocialSecurityNumber();
    }

    /**
     * Allows the patient to get diagnosed.
     * @param patientDiagnosis the diagnosis the patient gets diagnosed with
     */

    @Override
    public void setDiagnosis(String patientDiagnosis) {
        diagnosis = patientDiagnosis;
    }
}
