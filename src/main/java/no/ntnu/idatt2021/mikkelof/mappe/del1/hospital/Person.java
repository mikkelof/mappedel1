package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital;

/**
 * Abstract class for the Person type.
 * @author Mikkel Ofrim
 * @version 1.0.0
 */

public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    /**
     * Method that combines the first and last name into a single string
     * @return String containing both the first and last name combined
     */

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    @Override
    public String toString(){
        return "Name: " + getFullName() + "\nSocial security number: " + getSocialSecurityNumber();
    }


}
