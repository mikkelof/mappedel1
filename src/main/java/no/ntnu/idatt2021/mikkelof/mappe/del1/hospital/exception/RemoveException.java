package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.exception;

/**
 * Exception class
 * @author Mikkel Ofrim
 * @version 1.0.0
 */

public class RemoveException extends Exception {

    /**
     * Creates a new exception that throws the specified message when called
     */

    public RemoveException() {
        super("The person you tried removing is not in the register!");
    }

    //??
    long serialVersionUID = 1L;
}
