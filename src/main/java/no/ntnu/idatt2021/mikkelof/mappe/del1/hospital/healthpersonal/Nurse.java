package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.healthpersonal;

import no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.Employee;

/**
 * Class for the Nurse type.
 * @author Mikkel Ofrim
 * @version 1.0.0
 */

public class Nurse extends Employee {

    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString(){
        return "Nurse name: " + getFullName() + "\nSocial security number: " + getSocialSecurityNumber();
    }
}
