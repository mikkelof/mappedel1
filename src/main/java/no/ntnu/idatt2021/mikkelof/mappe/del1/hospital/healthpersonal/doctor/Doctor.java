package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.healthpersonal.doctor;

import no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.Employee;
import no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.Patient;

/**
 * Abstract class used by the Surgeon and GeneralPractitioner classes.
 * @author Mikkel Ofrim
 * @version 1.0.0
 */

public abstract class Doctor extends Employee {
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
