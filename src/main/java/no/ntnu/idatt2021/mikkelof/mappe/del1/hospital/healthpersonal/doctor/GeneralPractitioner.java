package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.healthpersonal.doctor;

import no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.Patient;

/**
 * Class for the General Practitioner type.
 * @author Mikkel Ofrim
 * @version 1.0.0
 */

public class GeneralPractitioner extends Doctor{
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Allows the general practitioner to set a diagnosis on a patient.
     * @param patient the patient the general practitioner wants to diagnose
     * @param diagnosis the diagnosis the general practitioner wants to set
     */

    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
