package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.healthpersonal.doctor;

import no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.Patient;

/**
 * Class for the Surgeon type.
 * @author Mikkel Ofrim
 * @version 1.0.0
 */

public class Surgeon extends Doctor{
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Allows the surgeon to set a diagnosis on a patient.
     * @param patient the patient the surgeon wants to diagnose
     * @param diagnosis the diagnosis the surgeon wants to set
     */

    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
