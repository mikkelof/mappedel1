package no.ntnu.idatt2021.mikkelof.mappe.del1.hospital;

import no.ntnu.idatt2021.mikkelof.mappe.del1.hospital.exception.RemoveException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class DepartmentTest {

    Department testDepartment;
    Employee testEmployee1;
    Employee testEmployee2;
    Patient testPatient1;
    Patient testPatient2;

    @BeforeEach                 //BeforeEach siden objekter vil bli fjernet og må derfor legges til på nytt i noen tilfeller
    public void setupAll() {
        testDepartment = new Department("Test Department");
        testEmployee1 = new Employee("Jon", "Jonnn", "5432324132");
        testEmployee2 = new Employee("Jan", "Jannn", "3451252343");
        testPatient1 = new Patient("John", "Johnson", "1083746372");
        testPatient2 = new Patient("Jane", "Janeson", "1462738473");
        testDepartment.addEmployee(testEmployee1);
        testDepartment.addPatient(testPatient1);
    }

    @Nested
    @DisplayName("Tests the remove method")
    class remove {
        @Test
        public void shouldThrowAnExceptionWhenEmployeeDoesNotExist() throws RemoveException {
            Assertions.assertThrows(RemoveException.class, () -> {
                testDepartment.remove(testEmployee2);
            });
        }

        @Test
        public void shouldBeRemovedWhenEmployeeDoesExist() throws RemoveException {
            int startSize = testDepartment.getEmployees().size();
            testDepartment.remove(testEmployee1);
            Assertions.assertEquals(startSize - 1, testDepartment.getEmployees().size());
        }

        @Test
        public void shouldThrowAnExceptionWhenPatientDoesNotExist() {
            Assertions.assertThrows(RemoveException.class, () -> {
                testDepartment.remove(testPatient2);
            });
        }

        @Test
        public void shouldBeRemovedWhenPatientDoesExist() throws RemoveException {
            int startSize = testDepartment.getPatients().size();
            testDepartment.remove(testPatient1);
            Assertions.assertEquals(startSize - 1, testDepartment.getPatients().size());
        }
    }
}
